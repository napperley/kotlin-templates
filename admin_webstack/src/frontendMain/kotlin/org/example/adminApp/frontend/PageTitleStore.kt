package org.example.adminApp.frontend

import dev.fritz2.binding.RootStore

internal object PageTitleStore : RootStore<String>("Admin") {
    override val update = handle { _, action: String ->
        changePageTitle(action)
        action
    }
}
