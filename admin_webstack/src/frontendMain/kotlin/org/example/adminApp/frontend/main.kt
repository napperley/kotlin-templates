package org.example.adminApp.frontend

import dev.fritz2.binding.const
import dev.fritz2.binding.handledBy
import dev.fritz2.dom.html.HtmlElements
import dev.fritz2.dom.html.render
import dev.fritz2.dom.mount
import kotlinx.coroutines.flow.collectLatest

fun main() {
    render { mainLayoutContent() }.mount("main-layout")
}

private fun HtmlElements.mainLayoutContent() = div(id = "content") {
    h3 { text("Admin Server") }
    br {}
    secretLink()
    br {}
    manageLink()
}

private fun HtmlElements.secretLink() = a {
    href = const("#")
    text("Secret")
    clicks.map {
        var linkTxt = ""
        text.collectLatest { linkTxt = it }
        "Admin - $linkTxt"
    } handledBy PageTitleStore.update
}

private fun HtmlElements.manageLink() = a {
    href = const("#")
    text("Manage Users")
    clicks.map {
        var linkTxt = ""
        text.collectLatest { linkTxt = it }
        "Admin - $linkTxt"
    } handledBy PageTitleStore.update
}
