package org.example.adminApp.frontend

import kotlinx.browser.document

internal fun changePageTitle(title: String) {
    println("New Title: $title")
    document.getElementById("page-title")?.textContent = title
}
