package org.example.adminApp.server.page

import io.ktor.application.*
import io.ktor.html.*
import kotlinx.html.*

internal suspend fun ApplicationCall.createHomePage() = respondHtml {
    createHead()
    createBody()
}

private fun HTML.createHead() = head {
    title {
        id = "page-title"
        +"Admin"
    }
}

private fun HTML.createBody() = body {
    createMainLayout()
    createScript()
}

private fun BODY.createScript() = script {
    type = "application/javascript"
    src = "js/admin-webstack.js"
}

private fun BODY.createMainLayout() = div {
    id = "main-layout"
    +"Loading..."
}
