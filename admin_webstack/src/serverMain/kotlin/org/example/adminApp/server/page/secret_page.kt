package org.example.adminApp.server.page

import io.ktor.application.*
import io.ktor.html.*
import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head
import kotlinx.html.title

internal suspend fun ApplicationCall.createSecretPage() = respondHtml {
    createHead()
    body {
        +"Secret Page"
    }
}

private fun HTML.createHead() = head { title("Admin Server - Secret") }
