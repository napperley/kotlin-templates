package org.example.adminApp.server

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.slf4j.LoggerFactory

private const val PUBLIC_HOST = "0.0.0.0"
private const val PUBLIC_HTTP_PORT = 8080
private val logger by lazy { LoggerFactory.getLogger("Server Main") }
// TODO: Fetch usernames from a DB.
private const val username = "test"
// TODO: Fetch passwords from a DB.
private const val password = "test"

fun main() {
    logger.info("Server access: http://$PUBLIC_HOST:$PUBLIC_HTTP_PORT")
    embeddedServer(Netty, createAppEngineEnvironment()).start(true)
}

private fun createAppEngineEnvironment() = applicationEngineEnvironment {
    module {
        main()
    }
    publicHttpApiConnector()
}

private fun ApplicationEngineEnvironmentBuilder.publicHttpApiConnector() = connector {
    host = PUBLIC_HOST
    port = PUBLIC_HTTP_PORT
}

private fun Application.main() {
    install(Authentication, setupBasicAuthentication())
    routing {
        basicAuthRoutes()
        staticRoute()
        homeRoute()
    }
}

private fun setupBasicAuthentication(): Authentication.Configuration.() -> Unit = {
    basic("basicAuth") {
        realm = "Admin Server"
        validate { credentials ->
            // TODO: Validate credentials using a DB.
            if (credentials.name == username && credentials.password == password) UserIdPrincipal(credentials.name)
            else null
        }
    }
}
