package org.example.adminApp.server

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.content.*
import io.ktor.routing.*
import org.example.adminApp.server.page.createHomePage
import org.example.adminApp.server.page.createSecretPage

internal fun Route.homeRoute() = get("/") {
    call.createHomePage()
}

internal fun Routing.staticRoute() = static {
    resources("static")
}

private fun Route.secretRoute() = get("secret") {
    call.createSecretPage()
}

internal fun Routing.basicAuthRoutes() = authenticate("basicAuth") {
    secretRoute()
}
