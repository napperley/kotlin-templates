import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("multiplatform") version "1.4.10"
    id("dev.fritz2.fritz2-gradle") version "0.7.1"
    application
}

application {
    mainClassName = "org.example.adminApp.server.MainKt"
}

repositories {
    jcenter()
    mavenCentral()
}

kotlin {
    jvm("server") {
        compilations.getByName("main") {
            dependencies {
                val ktorVer = "1.4.0"
                val slf4jVer = "1.7.30"
                implementation("io.ktor:ktor-server-core:$ktorVer")
                implementation("io.ktor:ktor-server-netty:$ktorVer")
                implementation("io.ktor:ktor-auth:$ktorVer")
                implementation("io.ktor:ktor-html-builder:$ktorVer")
                implementation("org.slf4j:slf4j-log4j12:$slf4jVer")
            }
        }
        // Allow inclusion of Java source files to ensure the application plugin works properly.
        withJava()
    }
    js("frontend") {
        browser()
        binaries.executable()
    }
}

val copyJsFiles = tasks.create<Copy>("copyJsFiles") {
    dependsOn("frontendBrowserDistribution")
    from("$buildDir/distributions")
    into("src/serverMain/resources/static/js")
    include("*.js")
}

tasks.create("runServer") {
    dependsOn(copyJsFiles, "run")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}